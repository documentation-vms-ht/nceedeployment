<#
This File Extracts the  AsciiDoc == Section Headers and creates a List of the CrossReferences between the [[ ]] characters
Specify the -File parameter to assign the file.
Outputs an Ascii Unordered List of Cross References.
#>


param(
    $File = ""
)

$a = Select-String $File -Pattern "=="
$list = New-Object Collections.Generic.List[String]

foreach ($s in $a) {
    $p = $s.toString().Split(":"); 
    $list.Add($p[3])
}
$more = New-Object Collections.Generic.List[String]
foreach ($l in $list){
    $sp = $l.Split("[[")
    $more.Add($sp[2])
    
}

foreach ($m in $more){
    $o = $m.Replace("]]",">>")
    Write-Host "*  <<$o"
}


