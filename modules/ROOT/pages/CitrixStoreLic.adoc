
= Citrix Licensing 

The NCEE System makes use of a Shared Citrix Server which is external to the Main Resources

There are 2 license servers (primary and backup) with Azure Traffic Manager in front of it. If Primary license server goes down, traffic manager automatically routes the traffic to Backup license server.


The Azure Resource Group Name is *CitrixLicense* is the RG in Azure


*Primary License server:  (West US Azure region)* +
RDP to citrixlic-dev.westus.cloudapp.azure.com +
Connect as: installer / V@rian01V@rian01 +

*Backup License server: (East US Azure region)* +
RDP to citrixlicbak-dev.eastus.cloudapp.azure.com +
Connect as: installer / V@rian01V@rian01 +

== Clear License Allocation

Reference: link:https://helgeklein.com/blog/2012/12/batch-delete-issued-citrix-userdevice-licenses-with-udadmin[Batch Delete Issued Citrix User/Device Licenses with Udadmin]

Log into the License Server Above

=== To get a list of currently issued licenses run it like this:

....
    C:\Program Files\Citrix\Licensing\LS>udadmin.exe -list
....

=== You can delete individual license assignments like this:

....
    udadmin -f FEATURE [-device | -user] NAME -delete
....

Unfortunately there is no built-in command that deletes all issued licenses. 

=== A one-liner batch script adds that missing functionality for device…

....
    for /f "tokens=1,2" %i in ('udadmin -list ^| find /i "_ud"') do @udadmin -f %j -device %i -delete
....
=== …and user licenses:

....
    for /f "tokens=1,2" %i in ('udadmin -list ^| find /i "_ud"') do @udadmin 
....

If you have both types of licenses, just run both commands one after the other.