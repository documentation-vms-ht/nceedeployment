= Troubleshooting


=== Error -  Deployment Failed.

==== Symptom
----
WARNING: All virtual machines in this resource group are not stopped.  Please stop all VMs and try again
----
==== Solution
The MASTER System is still running.  Please Stop and start again.

==== Symptom
----
    VERBOSE: Creating Virtual Machine MASTER-TBOX in resource group Leon at location eastus
    New-AZVM : Long running operation failed with status 'Unauthorized'.
    ErrorCode: ExpiredAuthenticationToken
    ErrorMessage: The access token expiry UTC time '2/27/2020 3:12:09 PM' is earlier than current UTC time '2/27/2020 3:12:16 PM'.
    ErrorTarget: 
    StatusCode: 401
----
==== Solution
bad Timing - Remove the Resource group manually (clean up elsewhere depending on how far it got.) if needed, and try again.



==== Error - Can't edit the Webpage.

On the Webdeploy - System you can't edit the Values.  The status message is as follows:
----
Status:    Please pass a name on the query string or in the request body.

==== Solution
