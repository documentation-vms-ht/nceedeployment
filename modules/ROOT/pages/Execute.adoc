= Execute a Deployment 

== On this page

*   <<Powershell>>
*   <<Webpage>>


== From Powershell [[Powershell]]

....
    RemoteVMAdmin.ps1
        [-ResourceGroup] <String>]
        [-Location <String>]  (Optional)
        [-NumUsers <String>]  (Optional)
        [-PasswordPattern <string>]  (Optional)
        [-Action <String> (create or delete)]
....

=== Parameters

* *ResourceGroup* +
The name of the intended Spoke System.  This Name should be limited to a maximum of 8 characters.  The name will be used to created the Azure Resource Group, Spoke Hostname (ResourceGroup-TBOX), Vnet Peering ID, Spoke User Names (HUB\ResourceGroup[1-15]), Citrix Machine Catalog , Delivery Group and Application Folder.  This Name needs to be unique. The script will check and ensure the name is available as a first step

* *Location* +
This has been now hardcoded for SDE, as that is the only system in use currently. It is left here in case in future it is needed. +
Specify the Location of the HUB – Current options at time of writing are SDE (US), APAC, OMSTEST, OMSDEV.  This setting indicates to the Script which HUB system The Spoke will be connected to. 

* *NumUsers*
Specify the Number of Demo Users to Create.  If not specified explicitly, then 15 users are created by default.

* *PasswordPattern*
Specify a Password Pattern that all demo users will be assigned.  Each demo user created has a number appended to the end of the user name.  The Password pattern for each user will be this PasswordPattern with the users number appended to the end.  If not Specified - this is *VarianDemo!!*  - which means for user *SDE\test12* for instance, the Password would be *VarianDemo!!12*

* *Action* +
Specify “create” to create a Cloned Spoke System.  Specify “delete” to clean up a previous installation.  Delete removes the Citrix Application Folder, Delivery Group and Machine Catalog. IT removes the Users and Groups created in the Hub-DC.  It deletes the Azure Resource Group and removes the Peering entry from the HUB-VNet settings. 



== From Webpage [[Webpage]]

The Proof of Concept website is a very basic website to prove funtionality +

You need be granted permission by the Developer ( link:mailto:henry.trusiak@varian.com[email ]).


link:https://deploywhiz.azurewebsites.net/[]

WARNING: The website Currently only deploys the *ARIA 15.6* Environment.  This may be updated at a later time.

image::ROOT:websiteDeploy.png[align="center" 600]


From the above image:

. *Resource Group Name*:  +
    This will be the name of the Demo System to be used.  Alpha characters only up to 8 characters. 
. *Does this RG Exist*:   +
    Use this Button to confirm that the Resource Group is not taken already.
. *SubNetID*:  +
    Give a value between 10 and 253. this is the Unique Number to used as the 3rd octet in the Cloned Spokes IP address.  You can use the Utility Function *GetNextUniqueIP.ps1* to get this value.  This utility may be implemented on this website in a future release.
. *Is this ID Unique*: +
 Use this Button to confirm that the SubnetID is correct.
. *Deploy!!*:  +
If the two Text boxes are filled then this will attempt to start a deployment.  It will test to ensure uniqueness. 
. *Output*
Deployment Output is displayed in this Box. 
