= Manually Creating Scheduled StartUp and Shutdown of the Cloned Spoke


Previously, this was not an automated step for an automatic startup and shutdown procedure.  
However - this step is now automated in the Main Script and can be run manually using the *Scripts\ScheduleCreateDelete.ps1* Script.

However - for legacy and other reasons, you may still need to manually create a Scheduled Automated Start/Stop VM 

Therefore - instructions are listed below.

It is recommended to set the time for startup at 7am and shutdown at 7pm.  Ensure the Timezone is set to the local time of where the Cloned Spoke is intended to be used

== Instructions

. Access the link:https://portal.azure.com[Azure Portal]

. Navigate to the *sde-automation* Resource Group
+
image::ROOT:SelectSDEAuto.png[align="center", 600]

. Select the *StartStopVMs* Runbook
+
image::ROOT:SelectStartStopVMs.png[align="center", 600]
. Select *Schedules*
+
image::ROOT:SelectSchedules.png[align="center", 600]

NOTE: There are two parts to this Process.  1) Create a Schedule to start and a Schedule to Stop. 2) Configure Parameters and Run settings for each schedule Created

. Click *Add a Schedule*
+
image::ROOT:AddASched.png[align="center", 600]

+

. From the *Schedule Runbook Window* Click *Link a schedule to your runbook*.
+
image::ROOT:LinkASchedule.png[align="center", 300]
. Click *Create a new Schedule*
+
image::ROOT:CreateSchedule.png[align="center", 300]
. In the *New Schedule* window Add the Following Parameters:
+
image::ROOT:NewSchedule.png[align="center", 300]
.. *Name:*  Add something Meaningful to the Name - I suggest {ClonedSpoke Name}  Wake or {ClonedSpoke}  Sleep depending on its purpose.
.. *Description*  Optional
.. *Starts*  Set a Start Time for the Event +
+
NOTE: If it is a a Start Schedule -then set to 7am.  For shutdown, set 7pm.  Ensure you choose a Time Zone appropriate for where the Cloned Spoke is being used.
+
.. *Time zone*  Set an appropriate for where the Cloned Spoke is being used.
.. *Select *Recurring*
.. *Recur Every*  Set for *1 Day*  
.. *Set Expiration* Set to No.
.. Click *Create*
+
NOTE:  It has been found that setting this setting makes no difference, the Schedule never expires.
+

. Select *Configure parameters and run settings*
+
image::ROOT:ParameterSched.png[align="center", 300]
. Edit the Following Entries:
.. *VMNAME* -  Set this to MASTER-TBOX.  This is the VM Name in the ClonedSpoke Azure Resource Group that cannot be renamed.
.. *RESOURCEGROUPNAME*  -  This is the name of the Cloned Spoke System.
.. *VMACTION*   - Enter *Startup* for Wake/Startup  or *Shutdown* for Sleep/shutdown schedules.
.. Click *OK*
. Click *OK* on the *Schedule Runbook* window
NOTE:  Ensure you create a schedule and Configure Parameters for Startup/Wake and Shutdown/Sleep