= CHANGELOG

===== TODO

*  [ ] Update the Deploy Website and add new options
*  [x] Upload Files/Changes to Linux Deploy Server.
** [x] Add Changes needed to files in Linux to a section. 
*  [ ] Add option to Website for 15.6 and 16.0 deploy options
*  [ ] Email output at the end of Deployment with details ?
*  [x] Add screenshot of NCEE output. (Scripts Cloned Spoke)

===== *Feb 27, 2020
* Updated Procedure for uploading files to Linux.
* Updated the Deploy.sh script to allow for 15.6 or 16 deployment.


===== *Feb 26, 2020*

*  Added New Azure Scheduled Runbook Automation script details  
*  Added NumUsers and Password Pattern to Script details
*  Location Parameter (Mainscript) now hardwired to SDE for now.
*  SubNetID Parameter no longer needed (Mainscript).  Autoassigned.



===== *Feb 20, 2020*

*  Initial Release.  Many Items not completed on Index.

