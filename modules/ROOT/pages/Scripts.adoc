== Scripts used in the System

The following Lists the Files used in the NCEE Deployment System

[%autofit]
----
    ROOT
    |   Clone-AzResourceGroup.ps1
    |   Clone-AzureRMresourceGroup.ps1
    |   RemoteVMAdmin-1563.ps1
    |   RemoteVMAdmin.ps1
    |   SetCred.ps1
    |
    +---Extras
    |   +---CTXC
    |   |       CTXCDeploy.ps1
    |   |       SetCTXRG-Icons.ps1
    |   |       
    |   +---DC1
    |   |       APACVMCred.xml
    |   |       CreateDemoAD.ps1
    |   |       
    |   \---MASTER
    |           DisableCredSSP.ps1
    |           EnableClientSSP.ps1
    |           EnableCredSSP.ps1
    |           InstallVDA.ps1
    |           ReinstallSWLocal.ps1
    |           ReinstallSWLocal155.ps1
    |           
    +---Functions
    |   |   ChangeCTXIcons.ps1
    |   |   DeploySpoke.ps1
    |   |   DoesResourceGroupExist.ps1
    |   |   GetAllSDEResourceGroups.ps1
    |   |   GetDeployLog.ps1
    |   |   GetNextUniqueIP.ps1
    |   |   GetSDEPeerings.ps1
    |   |   IsDeployInProgress.ps1
    |   |   IsTboxRunning.ps1
    |   |   NCEEStatus.ps1
    |   |   Set-SDERGCTXIcons.ps1
    |   |   SetCTXRG-Icons.ps1 
    |          
    +---Scripts
    |   |   AddDataAdminUsers.ps1
    |   |   AddSFUsers.ps1
    |   |   AddVMtoHUBDomain.ps1
    |   |   Cleanup.ps1
    |   |   Clone-AzResourceGroup.ps1
    |   |   CreateDemoAD.ps1
    |   |   CreateUserDCRemote.ps1
    |   |   CTXCAddOrDelete.ps1
    |   |   DeleteUserDCRemote.ps1
    |   |   DeploySpoke.sh
    |   |   GetNextUniqueIP.ps1
    |   |   Get-APACPeerings.ps1
    |   |   Get-Peerings.ps1 
    |   |   GetDeployLog.sh
    |   |   InstallVDA.ps1
    |   |   NCEEStatus.ps1
    |   |   RemoveVMfromDomain.ps1
    |   |   ScheduleCreateDelete.ps1
    |   |   Set-AutoLogon.ps1
    |   |   SetDCFLocation-1563.ps1
    |   |   SetDCFLocation.ps1
    |   |   TestRGExists.ps1
    |   |   UninstallAppsRemote-1563.ps1
    |   |   UninstallAppsRemote.ps1
    |
    \----- 
----

== Folders

=== ROOT Folder
The Root folder contains the Main File used to deploy a Cloned Spoke  `RemoteVMAdmin.ps1`. +
It also contains utilities to Copy and Clone a Resource Group if needed. +
The `SetCred.ps1` Script is used to Set the Password for the Installer user, so that deployment scripts to impersonate the installer user when executing certain tasks.  This script is used typically when setting a new Environment, or the installer user has changed.
More details at (LINK HERE TODO) Scripts that Run from Laptop or Server (Main)

=== Scripts Folder
This folder contains the scripts that called to execute from the main script `RemoteVMAdmin.ps1` +
They can be also be used individually to Invoke a command on the remote Spoke.
More Details at Link Scripts that execute on the Cloned Spoked

=== Extras Folder
This folder does not contain scripts that are executed from the Local Laptop or Workstation. +
These script files are files that are in place on the respective Servers, to be called by other scripts to do their tasks.
The CTXC refers to the Hub Citrix Controller, DC1 the Hub Domain Controller and the MASTER, the Master-TBOX server before it is cloned.
More details at Link Scripts that execute on the MASTER

=== Functions Folder
This contains Powershell scripts that call Azure Functions to do certain functions. They are mainly designed for webhooks to call their tasks.  They are used in some scripts in the system, but are used primarily on the Website to deploy the resources.
More details at link Azure Functions





