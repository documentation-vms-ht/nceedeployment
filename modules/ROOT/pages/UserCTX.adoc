= Demo User access to Cloned Spoke

Demo Users (ie Customers) can access The Cloned Spoke systems via Citrix Netscaler / Storefront. +

When a Cloned Spoke is deployed, by default 15 demo users are created.

Therefore, if the Cloned Spoke was called MULTI, then user SDE\MULTI1 through SDE\MULTI15 would be created.

The Password pattern by default is  VarianDemo!!{?}  where {?} is the Number used in the username

----

ie
    User:     SDE\MULTI10
    Password: VarianDemo!!10

----
image::ROOT:NetscalerLogon.png[align="center", 600]

The NCEE Demo Netscaler URL is:   link:amsdemo.cloud.varian.com[]

By default, under the *Apps* Tab in the Storefront, Three Varian Applications are published by default - *Home*, *Data Administration* and *Velocity*.  The Cloned Spoke Name is appended to the Published Application name.

image::ROOT:Storefront.png[align="center", 600]

It is possible to manually publish more applications as needed (as seen in the Screenshot).  This can be done from the Citrix Controller.  See the relevant section to do this.
